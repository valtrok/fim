import re

FORMAT_SPECIFIER_REGEX = r'.?[<>]?[-^=+ ,_.bcdeEfFgGoxXnlu%0-9]+'


class Formatter:
    def __init__(self, template_string):
        self.slots = []
        for k in template_string.split('{'):
            if re.match(
                    r'.*\$<(?:' + FORMAT_SPECIFIER_REGEX + ':)?[_a-z0-9]+>.*}',
                    k):
                self.slots.append({
                    'template':
                    k.split('}')[0],
                    'key':
                    re.findall(
                        r'\$<((?:' + FORMAT_SPECIFIER_REGEX +
                        ':)?[_a-z0-9]+)>', k)[0]
                })
        self.template_string = template_string

    def _format_string(self, string, form):
        if form == 'u':
            return str(string).upper()
        if form == 'l':
            return str(string).lower()
        if form == 'c':
            return str(string).capitalize()
        return ('{:' + form + '}').format(string)

    def compute(self, **kwargs):
        string = self.template_string[:]
        for s in self.slots:
            if ':' in s['key']:
                key = s['key'].split(':')[1]
            else:
                key = s['key']
            if key in kwargs.keys():
                if ':' in s['key']:
                    formatted = self._format_string(kwargs[key],
                                                    s['key'].split(':')[0])
                else:
                    formatted = str(kwargs[s['key']])
                string = string.replace(
                    '{' + s['template'] + '}',
                    s['template'].replace('$<' + s['key'] + '>', formatted))
            else:
                string = string.replace('{' + s['template'] + '}', '')
        return string


def format(template_string, **kwargs):
    return Formatter(template_string).compute(**kwargs)
