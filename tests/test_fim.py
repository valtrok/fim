import fim


def is_format_compliant(f, a):
    return fim.Formatter('{$<' + f + ':a>}').compute(a=a) == ('{:' + f +
                                                              '}').format(a)


class TestFim:
    def test_slots(self):
        formatter = fim.Formatter('{$<a> }{<<$<b>>>}{--$<c>--}')
        a = '1'
        b = '2'
        c = '3'
        assert formatter.compute(
            a=a, b=b, c=c) == str(a) + ' <<' + str(b) + '>>--' + str(c) + '--'
        assert formatter.compute(a=a, b=b) == str(a) + ' <<' + str(b) + '>>'
        assert formatter.compute(a=a, c=c) == str(a) + ' --' + str(c) + '--'
        assert formatter.compute(a=a) == str(a) + ' '
        assert formatter.compute(b=b,
                                 c=c) == '<<' + str(b) + '>>--' + str(c) + '--'
        assert formatter.compute(b=b) == '<<' + str(b) + '>>'
        assert formatter.compute(c=c) == '--' + str(c) + '--'
        assert formatter.compute() == ''

    def test_basic_formatters(self):
        a = 'saLut'
        assert fim.Formatter('{$<u:a>}').compute(a=a) == a.upper()
        assert fim.Formatter('{$<c:a>}').compute(a=a) == a.capitalize()
        assert fim.Formatter('{$<l:a>}').compute(a=a) == a.lower()
        assert fim.Formatter('{$<a>}').compute(a=a) == a

    def test_format_padding(self):
        a = 'salut'
        for f in ['>7', '<7', '_<7', '^7', '^7', '_^8']:
            assert is_format_compliant(f, a)

    def test_format_truncate(self):
        a = 'salut'
        for f in ['.1', '.3', '5.2', '_<10.4']:
            assert is_format_compliant(f, a)

    def test_format_numbers(self):
        for a in [3, -4]:
            for f in ['d', '3d', '04d', ' d', '+d', '=2d', '=+3d']:
                assert is_format_compliant(f, a)

    def test_special_characters(self):
        a = 'salut'
        assert is_format_compliant('$<10', a)
        assert fim.Formatter('{$<$<$<10:a>}').compute(
            a=a) == '$<' + '{:$<10}'.format(a)
        assert fim.Formatter('{$<$<$<<10:a>}').compute(
            a=a) == '$<$<' + '{:10}'.format(a)

    def test_format_function(self):
        a = '1'
        b = '2'
        c = '3'
        assert fim.format(
            '{$<a> }{<<$<b>>>}{--$<c>--}', a=a, b=b,
            c=c) == str(a) + ' <<' + str(b) + '>>--' + str(c) + '--'
