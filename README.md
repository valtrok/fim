# fim - Format IMproved

[![pipeline status](https://gitlab.com/valtrok/fim/badges/master/pipeline.svg)](https://gitlab.com/valtrok/fim/pipelines)
[![coverage report](https://gitlab.com/valtrok/fim/badges/master/coverage.svg)](https://valtrok.gitlab.io/fim/coverage/)

fim is an advanced string formatter for python

# Usage

fim provides a string formatter class called `Formatter`

This class has to be created with a template string. It will then format strings according to this template.

## Basic usage

Import fim
```python
import fim
```

Instanciate the formatter with a template string
```python
formatter = fim.Formatter('Hello{ $<name>}!')
```

Call the `compute` function to compute strings with this template
```python
formatted_string = formatter.compute(name='John')
```

Which will result in `Hello John!`

For a one-shot call, you can use the `format` function:

```python
formatted_string = fim.format('Hello{ $<name>}!', name='John')
```

## Template format

The template string contains slots. A slot is a part of string that will conditionally appear in the result string.

Here is the basic format of a slot:

```
{before $<fmt:var> after}
```

The whole slot (from `{` to `}`) will only appear if you call `compute` with a parameter called `var`.

`fmt` is a format modifier used by the underlying format built-in (e.g. `02d`). This part is optional.

You can use `fmt` to format you text too (`u` will uppercase the variable, `l` lowercase and `c` capitalize).

## Advanced example

```python
import fim

formatter = fim.Formatter('Hello{ $<c:name>}!{ I can see that you are $<3d:age> years old!} Have a nice day!')

print(formatter.compute(name='john'))
print(formatter.compute(age=40))
print(formatter.compute(name='john', age=40))
print(formatter.compute())
```

will result in:

```
Hello John! Have a nice day!
Hello! I can see that you are  40 years old! Have a nice day!
Hello John! I can see that you are  40 years old! Have a nice day!
Hello! Have a nice day!
```

## Why this heavy syntax?

The slot syntax is quite heavy, but it is for a good reason. Two characters are needed to begin the "variable block" because if only one is used, then interferences with the format field can appear:

If we get rid of the `$` and we want something to be formatted with the specifier `<<10` (Aligned left with `<` characters to fill the space), then the slot will look like something like this:

```
{<<<10:var}
```

The formatter is now unable to know if you want your string to begin with `<` and be aligned left or if you want it to begin with `<<` and be padded by 10, and so on.

The `$` is allowing a lot more flexibility:

A slot like

```
{$<$<$<10:var}
```

Is directly interpreted as `$<` then your slot with `$<10` format specifier. If you want it to be interpreted as `$<$<` and then your slot with `10` format specifier, you can just add a `<` to make it explicit:

```
{$<$<$<<10:var>}
```
